#!/bin/sh

ICMMTS_PATH="${ICMMTS_PATH:-./icmmts}"

validate() {
    "$ICMMTS_PATH/validate.py"
}

render() {
    x="$1"
    shift 1
    echo "rendering $x : $@"
    "$ICMMTS_PATH/render.py" $@ | xmllint --format - > "$x"
}

mkdir -p out
validate || exit
render out/heightmap.svg
render out/biomemap.svg color=biome
render out/nationmap.svg color=nation nation_legends=1
render out/idmap.svg width=4000 height=2000 color=nation show_id=1
